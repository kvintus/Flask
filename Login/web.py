from flask import Flask, render_template, request, url_for, redirect
import sqlite3
import os.path

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
db_path = os.path.join(BASE_DIR, "users.db")
app = Flask(__name__, template_folder='templates')

@app.route('/logged')
def logged():
    return "LOgged in!"

@app.route('/')
def index():
    conn = sqlite3.connect(db_path)
    curr =conn.cursor()
    curr.execute('select * from users order by username')
    users = curr.fetchall()
    #print(users)
    return render_template('index.html', users = users)

@app.route('/login', methods = ['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')
    elif request.method == 'POST':
        conn = sqlite3.connect(db_path)
        curr = conn.cursor()
        if request.form.get('submit_field') == "Login":
            curr.execute('select password from users where username = "{}"'.format(request.form.get('username')))
            if curr.fetchone()[0]  == request.form.get('password') :
                return "Logged in!"
            else:
                return ('Login failed')
        elif request.form.get('submit_field') == "Register":
            curr.execute('insert into users (username, password) values("{}", "{}")'.format(request.form.get('username'), request.form.get('password')))
            conn.commit()
            return "Registred"



app.run(debug=True)